import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styled from "styled-components";
import { Paper, TableContainer } from '@material-ui/core';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


const Styles = styled.div`
 .react-datepicker-wrapper,
 .react-datepicker__input-container,
 .react-datepicker__input-container input {
   width: 175px;
 }

 .react-datepicker__close-icon::before,
 .react-datepicker__close-icon::after {
   background-color: grey;
 }
`;

export function DatePickerRange() {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [dates, setDates] = useState([]);

  const fetchData = async () => {
    try{
    const resp = await fetch("https://us-central1-fsdproject-338315.cloudfunctions.net/getDates");
    const data = await resp.json();
    
    setDates(data[0]);
    console.log(data);
    
    }
    catch(error)
    {
      console.log(error)
    }
  };


  useEffect(() => {
    fetchData();
   
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const body = {
        startDate, 
        endDate
      };

     
      const response = await fetch("https://us-central1-fsdproject-338315.cloudfunctions.net/saveDates", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({body})
      });
      console.log(response);
      
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <div style={{ display: "flex" }}>
        <DatePicker
          isClearable
          filterDate={d => {
            return new Date() > d;
          }}
          placeholderText="Select Start Date"
          showTimeSelect
          dateFormat="MMMM d, yyyy h:mmaa"
          selected={startDate}
          selectsStart
          startDate={startDate}
          endDate={endDate}
          onChange={date => setStartDate(date)}
        />
        <DatePicker
          isClearable
          filterDate={d => {
            return new Date() > d;
          }}
          placeholderText="Select End Date"
          showTimeSelect
          dateFormat="MMMM d, yyyy h:mmaa"
          selected={endDate}
          selectsEnd
          startDate={startDate}
          endDate={endDate}
          minDate={startDate}
          onChange={date => setEndDate(date)}
        />
        <form onSubmit={handleSubmit}>
          <button type="submit">Add picked dates</button>
        </form>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ maxWidth: 650}} aria-label="simple table">
          <TableHead sx={{ background: "#197278" }}>
            <TableRow>
              
              <TableCell align="right">startDate</TableCell>
              <TableCell align="right">endDate</TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ background: "#772e25" }}>
            {
              <TableRow
                
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
               
                <TableCell align="right">{dates.start_date}</TableCell>
                <TableCell align="right">{dates.end_date}</TableCell>
              </TableRow>
            }
          </TableBody>
        </Table>
      </TableContainer>

    </div>
  );
}
// function createData(startDate, endDate) {
//   return {startDate, endDate };
// }
// let rows = [
//   // createData(1, "2022-01-01", "2021-12-31"),
//   // createData(2, "2022-01-07", "2022-01-01"),
//   createData(dates[0].start_date, dates[0].end_date)
// ];

export default function TableDatePicker() {
  return (
    <Styles>
      <DatePickerRange />
    </Styles>
  );

}
